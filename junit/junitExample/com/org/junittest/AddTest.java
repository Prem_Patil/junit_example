package com.org.junittest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.org.junitexample.MyJunitClass;

public class AddTest {

	@Test
	public void Addtest() {
		MyJunitClass junit = new MyJunitClass();
		int result = junit.add(10, 20);
		
		assertEquals(30 , result);
	}

}
