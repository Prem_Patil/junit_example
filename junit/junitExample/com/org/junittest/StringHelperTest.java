package com.org.junittest;

import static org.junit.Assert.*;

import org.junit.Test;

import com.org.junitexample.StringHelper;

public class StringHelperTest {
	
	StringHelper junit = new StringHelper();

	@Test
	public void stringHelper_truncateAatFirst2places() {
		
		String result = junit.truncateAinFirst2Positions("AABC");
		assertEquals("BC", result);
	}
	
	@Test
	public void stringHelper_areFirstandLast2charsSame(){
		assertEquals(true, junit.areFirstandLast2charsSame("PPQQPP"));
	}

}
