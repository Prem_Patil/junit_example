package com.org.junittest;

import static org.junit.Assert.*;

import org.junit.Test;

import com.org.junitexample.MyJunitClass;

public class ConcatTest {

	@Test
	public void Concat() {
		MyJunitClass junit = new MyJunitClass();
			String result = junit.Concat("prem", " patil");
			assertEquals("prem patil", result);
	}

}
