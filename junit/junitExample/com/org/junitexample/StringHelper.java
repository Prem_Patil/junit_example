package com.org.junitexample;

public class StringHelper {
	
	public String truncateAinFirst2Positions(String str){
		
		if(str.length() <= 2){ return str.replaceAll("A", ""); }
		
		String first2char = str.substring(0 , 2);
		String stringMinusFirst2char = str.substring(2);
		return first2char.replaceAll("A", "") + stringMinusFirst2char;
	}
	
	public boolean areFirstandLast2charsSame(String str){
		
		if(str.length() <= 1){ return false; }
		if(str.length() == 2){ return true;  }
		
		String first2char = str.substring(0, 2);
		String last2char = str.substring(str.length() - 2);
		return first2char.equals(last2char);
	}

}
